<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cursos".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $temario
 * @property string|null $foto
 *
 * @property Contenido[] $contenidos
 * @property Matricula[] $matriculas
 */
class Cursos extends \yii\db\ActiveRecord
{

    public $archivo; //para almacenar el fichero de la foto



    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cursos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['nombre'], 'string', 'max' => 400],
            [['temario', 'foto'], 'string', 'max' => 200],
            [['id'], 'unique'],
            [['fecha'], 'safe'],


            [
                ['archivo'], 'file',
                'skipOnEmpty' => true, // no es obligatorio seleccionas un archivo
                'extensions' => 'jpg,bmp,gif,png' // extensiones permitidas
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'temario' => 'Temario',
            'foto' => 'Foto',
        ];
    }

    /**
     * Gets query for [[Contenidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContenidos()
    {
        return $this->hasMany(Contenido::class, ['idCurso' => 'id']);
    }

    /**
     * Gets query for [[Matriculas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculas()
    {
        return $this->hasMany(Matricula::class, ['idCurso' => 'id']);
    }


















    public function subirArchivo(): bool
    {
        $this->archivo->saveAs('imgs/cursos/' . $this->id . $this->archivo->name);
        return true;
    }



    /**
     * antes de validar cojo los archivos enviados y
     * los coloco en el modelo
     * 
     */
    public function beforeValidate()
    {
        //si he seleccionado un archivo en el formulario

        if (isset($this->archivo)) {

            $this->archivo = \yii\web\UploadedFile::getInstance($this, 'archivo');
        }
        return true;
    }

    public function afterValidate()
    {

        //si he seleccionado un archivo en el formulario
        if (isset($this->archivo)) {


            $this->subirArchivo();

            $this->foto = $this->id .  $this->archivo->name;
        }

        return true;
    }


    /**
     * afterSave
     *
     * @param  mixed $insert
     * @param  mixed $atributosAnteriores
     * @return void
     */
    public function afterSave($insert, $atributosAnteriores)
    {

        if (!$insert) {

            if (isset($this->archivo)) {

                if (isset($atributosAnteriores["foto"]) && $atributosAnteriores["foto"] != "") {

                    unlink('imgs/cursos/' . $atributosAnteriores["foto"]);
                }
            }
        }
    }

    public function afterDelete()
    {
        unlink('imgs/cursos' . $this->foto); //elimino la imagen de la noticia, cuando borro la noticia
    }
}
