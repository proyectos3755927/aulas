<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "matricula".
 *
 * @property int $id
 * @property int|null $idAlumno
 * @property int|null $idCurso
 * @property int|null $nota
 * @property int|null $apto
 * @property string|null $comentario
 * @property string|null $fechaCalificacion
 * @property string|null $fechaMatricula
 *
 * @property Alumnos $idAlumno0
 * @property Cursos $idCurso0
 */
class Matricula extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'matricula';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idAlumno', 'idCurso', 'nota', 'apto'], 'integer'],
            [['fechaCalificacion', 'fechaMatricula'], 'safe'],
            [['comentario'], 'string', 'max' => 400],
            [['idAlumno', 'idCurso', 'fechaMatricula'], 'unique', 'targetAttribute' => ['idAlumno', 'idCurso', 'fechaMatricula']],
            [['idAlumno'], 'exist', 'skipOnError' => true, 'targetClass' => Alumnos::class, 'targetAttribute' => ['idAlumno' => 'id']],
            [['idCurso'], 'exist', 'skipOnError' => true, 'targetClass' => Cursos::class, 'targetAttribute' => ['idCurso' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idAlumno' => 'Id Alumno',
            'idCurso' => 'Id Curso',
            'nota' => 'Nota',
            'apto' => 'Apto',
            'comentario' => 'Comentario',
            'fechaCalificacion' => 'Fecha Calificacion',
            'fechaMatricula' => 'Fecha Matricula',
        ];
    }




    public function getAlumnos()
    {
        $alumnos = Alumnos::find()->all();
        return ArrayHelper::map($alumnos, 'id', 'nombre');
    }

    public function getCursos()
    {
        $cursos = Cursos::find()->all();
        return ArrayHelper::map($cursos, 'id', 'nombre');
    }



    /**
     * Gets query for [[IdAlumno0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdAlumno0()
    {
        return $this->hasOne(Alumnos::class, ['id' => 'idAlumno']);
        //return $this->hasOne(Alumnos::class, ['id' => 'nombre']);
    }

    /**
     * Gets query for [[IdCurso0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdCurso0()
    {
        return $this->hasOne(Cursos::class, ['id' => 'idCurso']);
        //return $this->hasOne(Cursos::class, ['id' => 'curso']);
    }
}
