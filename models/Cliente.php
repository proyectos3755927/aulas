<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cliente".
 *
 * @property int $idCliente
 * @property string|null $nombreCliente
 * @property string|null $apellidosCliente
 * @property string|null $emailCliente
 * @property string|null $fechaNacimientoCliente
 */
class Cliente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cliente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idCliente'], 'required'],
            [['idCliente'], 'integer'],
            [['fechaNacimientoCliente'], 'safe'],
            [['nombreCliente', 'emailCliente'], 'string', 'max' => 100],
            [['apellidosCliente'], 'string', 'max' => 200],
            [['idCliente'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idCliente' => 'Id Cliente',
            'nombreCliente' => 'Nombre Cliente',
            'apellidosCliente' => 'Apellidos Cliente',
            'emailCliente' => 'Email Cliente',
            'fechaNacimientoCliente' => 'Fecha Nacimiento Cliente',
        ];
    }
}
