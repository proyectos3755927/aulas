<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alumnos".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $email
 * @property string|null $password
 * @property string|null $telefono
 * @property string|null $foto
 * @property string|null $fecha
 *
 * @property Matricula[] $matriculas
 */
class Alumnos extends \yii\db\ActiveRecord
{


    public $archivo; //para almacenar el fichero de la foto


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alumnos';
        //return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['nombre'], 'string', 'max' => 300],
            [['apellidos'], 'string', 'max' => 400],
            [['email', 'foto'], 'string', 'max' => 200],
            [['password'], 'string', 'max' => 8],
            [['telefono'], 'string', 'max' => 20],

            [
                ['archivo'], 'file',
                'skipOnEmpty' => true, // no es obligatorio seleccionas un archivo
                'extensions' => 'jpg,bmp,gif,png' // extensiones permitidas
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'email' => 'Email',
            'password' => 'Password',
            'telefono' => 'Telefono',
            'foto' => 'Foto',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * Gets query for [[Matriculas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculas()
    {
        return $this->hasMany(Matricula::class, ['idAlumno' => 'id']);
    }









    public function subirArchivo(): bool
    {
        $this->archivo->saveAs('imgs/alumnos/' . $this->id . $this->archivo->name);
        return true;
    }



    /**
     * antes de validar cojo los archivos enviados y
     * los coloco en el modelo
     * 
     */
    public function beforeValidate()
    {
        //si he seleccionado un archivo en el formulario

        if (isset($this->archivo)) {

            $this->archivo = \yii\web\UploadedFile::getInstance($this, 'archivo');
        }
        return true;
    }

    public function afterValidate()
    {

        //si he seleccionado un archivo en el formulario
        if (isset($this->archivo)) {


            $this->subirArchivo();

            $this->foto = $this->id .  $this->archivo->name;
        }

        return true;
    }


    /**
     * afterSave
     *
     * @param  mixed $insert
     * @param  mixed $atributosAnteriores
     * @return void
     */
    public function afterSave($insert, $atributosAnteriores)
    {

        if (!$insert) {

            if (isset($this->archivo)) {

                if (isset($atributosAnteriores["foto"]) && $atributosAnteriores["foto"] != "") {

                    unlink('imgs/alumnos/' . $atributosAnteriores["foto"]);
                }
            }
        }
    }

    public function afterDelete()
    {
        unlink('imgs/alumnos' . $this->foto); //elimino la imagen de la noticia, cuando borro la noticia
    }
}
