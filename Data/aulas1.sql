﻿DROP DATABASE IF EXISTS aulas2023;
CREATE DATABASE aulas2023;
USE aulas2023;

CREATE TABLE cursos(
id int,
nombre varchar(400),
temario varchar(200),
foto varchar(200),
PRIMARY KEY(id)
);

CREATE TABLE alumnos(
id int AUTO_INCREMENT,
nombre varchar(300),
apellidos varchar(400),
email varchar(200),
password varchar(8),
telefono varchar(20),
foto varchar(200),
fecha timestamp DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY(id)
);

CREATE TABLE contenido(
id int AUTO_INCREMENT,
documento varchar(200),
foto varchar(200),
video varchar(300),
idCurso int,
PRIMARY KEY(id)
);

CREATE TABLE matricula(
id int AUTO_INCREMENT,
idAlumno int,
idCurso int,
nota int,
apto boolean,
comentario varchar(400),
fechaCalificacion date,
fechaMatricula timestamp,
PRIMARY KEY(id),
UNIQUE KEY(idAlumno,idCurso,fechaMatricula)
);

CREATE TABLE user(
id int NOT NULL AUTO_INCREMENT,
nombre varchar(255),
apellidos varchar(255),
email varchar(100),
authKey varchar(255),
activo int(1),
password varchar(255),
accessToken varchar(255),
telefono varchar(20),
foto varchar(200),
administrador tinyint(1),
fecha timestamp DEFAULT CURRENT_TIMESTAMP,
UNIQUE KEY (id), 

PRIMARY KEY(id)
);


ALTER TABLE matricula 
  ADD CONSTRAINT fkMatriculaAlumnos FOREIGN KEY (idAlumno) REFERENCES alumnos(id),
  ADD CONSTRAINT fkMatriculaCursos FOREIGN KEY (idCurso) REFERENCES cursos(id);

ALTER TABLE contenido
  ADD CONSTRAINT fkContenidoCursos FOREIGN KEY (idCurso) REFERENCES cursos(id);

/*ALTER TABLE calificacion
  ADD CONSTRAINT fkCalificacionAlumnos FOREIGN KEY (idAlumno) REFERENCES alumnos(id),
  ADD CONSTRAINT fkCalificacionCursos FOREIGN KEY (idCurso) REFERENCES cursos(id);*/



ALTER TABLE cursos 
ADD COLUMN fecha date;
