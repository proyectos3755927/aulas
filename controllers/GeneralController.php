<?php

namespace app\controllers;

use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class GeneralController extends Controller
{


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                //Acciones del contrololador con el control de cuentas de usuario
                //'only' => ['ejercicio3'],
                'only' => ['*'],
                'rules' => [
                    [
                        //acciones que puedes realizar los usuarios logueados
                        'actions' => [],
                        //'controllers' => ['site', 'cliente'],
                        'controllers' => ['site', 'curso', 'matricula'],
                        'allow' => true,
                        'roles' => ['@'], //que ha iniciado session
                        //'matchCallback'=> function(){}
                    ],


                    [
                        //acciones que puedes realizar los usuarios NO logueados
                        'actions' => ['index', 'login', 'error'],   //l u no logueado no puede hacer nada del controlador Cliente
                        'controllers' => ['site',],
                        'allow' => true,
                        'roles' => ['?'], //que no ha iniciado session
                    ],



                    // [
                    //     //acciones que puedes realizar los usuarios NO logueados
                    //     'actions' => [''], //l u no logueado no puede hacer nada del controlador Cliente
                    //     'controllers' => ['cliente'],
                    //     'allow' => true,
                    //     'roles' => ['?'], //que no hna iniciado session
                    // ],



                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
}
