<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Cursos;

class SiteController extends GeneralController
{


    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $consulta = Cursos::find();
        $datos = $consulta
            ->all(); //

        return $this->render('index', [
            "datos" => $datos,
            "titulo" => "Cursos"
        ]);
    }



    public function actionViewcurso($id)
    //public function actionverNoticia($idnoticia)
    {
        $consulta = Cursos::find()->where([
            "id" => $id
        ]); // select * from noticia where idNoticia=1

        $dato = $consulta->one();

        return $this->render("verCurso", [
            "dato" => $dato
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }



    public function actionEjercicio1()
    {
        return  $this->render("ejercicio1");
    }
    public function actionEjercicio2()
    {
        return  $this->render("ejercicio2");
    }
    public function actionEjercicio3()
    {
        return  $this->render("ejercicio3");
    }
}
