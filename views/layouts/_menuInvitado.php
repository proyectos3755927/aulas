<?php

use yii\bootstrap5\Nav;
use yii\helpers\Html;

echo Nav::widget([
    'options' => ['class' => 'navbar-nav'],
    'items' => [
        ['label' => 'Home', 'url' => ['/site/index']],
        //    ['label' => 'Ejercicio 3', 'url' => ['/site/ejercicio3']],
        ['label' => 'Login', 'url' => ['/site/login']],
        ['label' => 'Alumnos', 'url' => ['/alumnos/create']],


    ]
]);
