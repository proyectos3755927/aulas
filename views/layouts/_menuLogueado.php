<?php

use yii\bootstrap5\Nav;
use yii\helpers\Html;


echo Nav::widget([
    'options' => ['class' => 'navbar-nav'],
    'items' => [
        ['label' => 'Home', 'url' => ['/site/index']],
        //['label' => 'Ejercicio 1', 'url' => ['/site/ejercicio1']],
        //['label' => 'Ejercicio 2', 'url' => ['/site/ejercicio2']],
        //a['label' => 'Ejercicio 3', 'url' => ['/site/ejercicio3']],

        //['label' => 'Administración', 'url' => ['/cliente/index']],

        ['label' => 'Administracion', 'items' => [
            ['label' => 'Curso', 'url' => ['/curso/index']],
            ['label' => 'Alumnos', 'url' => ['/alumnos/index']],
            ['label' => 'Matricula', 'url' => ['/matricula/index']],

        ]],

        '<li class="nav-item">'
            . Html::beginForm(['/site/logout'])
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'nav-link btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'
    ]
]);
