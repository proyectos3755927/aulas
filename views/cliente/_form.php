<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Cliente $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="cliente-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idCliente')->textInput() ?>

    <?= $form->field($model, 'nombreCliente')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellidosCliente')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'emailCliente')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fechaNacimientoCliente')->input('date') ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>