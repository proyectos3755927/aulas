<?php

use yii\helpers\Html;
?>

<style>
    .img-thumbnail {
        transition: transform 0.3s ease;
    }

    .img-thumbnail:hover {
        transform: scale(1.1);
    }
</style>

<div class="col-lg-4 mb-3">

    <div><?= Html::img(
                "@web/imgs/cursos/{$dato->foto}", // ruta  + nombre
                [
                    //"class" => 'col-lg-8'
                    "class" => 'img-thumbnail'
                ] // atributos html
            )
            ?></div>

    <h5><?= $dato->nombre ?></h5>

    <div><?= Html::a(
                "Ver Curso completa", // texto del enlace
                [
                    'site/viewcurso', // controlador/accion
                    //'site/verNoticia', // controlador/accion
                    'id' => $dato->id // parametro a enviar por URL
                ],
                [
                    "class" => "btn btn-primary" // atributos
                ]
            ) ?>
    </div>
</div>
<br>
<br>
<br>