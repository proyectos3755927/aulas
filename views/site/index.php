<?php

/** @var yii\web\View $this */


use yii\helpers\Html;

$this->title = 'Cursos';
?>










<div class="site-index">

    <div class="jumbotron text-center bg-transparent mt-5 mb-5">
        <h1 class="display-4">Aulas On Line</h1>

        <p class="lead">Cursos de Formación</p>

    </div>

    <div class="body-content">


        <div class="row">
            <?php
            foreach ($datos as $dato) {
                echo $this->render("_curso", [
                    "dato" => $dato
                ]);
            }
            ?>
        </div>



    </div>
</div>