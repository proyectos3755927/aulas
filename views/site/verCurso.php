<?php

use yii\helpers\Html;
?>
<div>
    <h4>
        <?= $dato->nombre ?></h4>
</div>
<div>
    <?= $dato->temario ?>
</div>
<div>
    <style>
        .rounded-circle {
            height: 40px;
            width: 60px
        }
    </style>
    <?= Html::img(
        "@web/imgs/cursos/{$dato->foto}",
        [
            "class" => "img-fluid"
        ]
    ) ?>
</div>


<?= $dato->fecha ?>