<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Matricula $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="matricula-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idAlumno')->dropDownList($model->getIdAlumno0); ?>


    <?= $form->field($model, 'idCurso')->dropDownList($model->getIdCurso0()); ?>


    <?= $form->field($model, 'nota')->textInput() ?>

    <?= $form->field($model, 'apto')->textInput() ?>

    <?= $form->field($model, 'comentario')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fechaCalificacion')->textInput() ?>

    <?= $form->field($model, 'fechaMatricula')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>