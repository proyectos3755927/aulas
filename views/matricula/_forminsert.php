<?php

use Faker\Core\Number;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Matricula $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="matricula-form">

    <?php $form = ActiveForm::begin(); ?>



    <?= $form->field($model, 'idAlumno')->dropDownList($model->alumnos); ?>

    <?= $form->field($model, 'idCurso')->dropDownList($model->cursos); ?>

    <?= $form->field($model, 'fechaMatricula')->input('date'); ?>




    <?= $form->field($model, 'nota')->input('number') ?>






    <?= $form->field($model, 'apto')->dropDownList([1 => 'Apto', 0 => 'No Apto']); ?>


    <?= $form->field($model, 'comentario')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'fechaCalificacion')->input('date') ?>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>